using System;
using System.Linq;
namespace Looping {
  public class MyMethods {
    public static void SwitchMethod () {
      Console.WriteLine ("---Welcome to Looping task! \n 1.Looping 10 Natural Numbers, \n 2.Sum of 10 Natural Numbers, \n 3.Draw Rectangle, \n 4.Display a Triangle, \n 5.Right Angle Triangle, \n 6.Pyramid----------");
      Console.WriteLine ("Choose Which task Would you like to Run from the Above Tasks:");
      int taskNumber = Convert.ToInt32 (Console.ReadLine ());
      switch (taskNumber) {
        case 1:
          Loop10Num ();
          break;
        case 2:
          SumOf10Num ();
          break;
        case 3:
          DrawRectangle ();
          break;
        case 4:
          DrawTriangle ();
          break;
        case 5:
          DrawRighAngleTriangle ();
          break;
        case 6:
          DrawPyramid ();
          break;
        default:
          Console.WriteLine ("Error you have given a invalid task number as input");
          break;
      }
      Console.WriteLine ("Would you like to do Loopimg task again (Y/N)?:");
      string TaskYesOrNo = Console.ReadLine ().ToUpper ();
      if (TaskYesOrNo == "Y") {
        SwitchLoop (TaskYesOrNo);
      }
      TaskYesOrNo = "";
    }
    public static void SwitchLoop (string TaskYesOrNo) {
      while (TaskYesOrNo == "Y") {
        SwitchMethod ();
        TaskYesOrNo = "";
      }
    }

    public static void Loop10Num () {
      Console.WriteLine ("------Task-1 find 1st ten Natural Numbers:----");
      int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
      Console.WriteLine ("Output:");
      int i = 0;
      for (i = 0; i < 11; i++) {
        Console.Write (i + " ");
      }
      Console.WriteLine ("--------------------------------------");
    }
    public static void SumOf10Num () {
      Console.WriteLine ("------Task-2 Sum ten Natural Numbers:----");
      int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
      Console.WriteLine ("Output:");
      int i = 0;
      int sum = 0;
      for (i = 0; i < 11; i++) {
        sum += i;
      }
      Console.WriteLine ("The Original Value: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ");
      Console.WriteLine ("the Sum of 1st 10 Natural Numbers is: " + sum);
      Console.WriteLine ("--------------------------------------");
    }
    public static void DrawRectangle () {
      Console.WriteLine ("------Task-3 Draw Rectangle:----");
      int x;

      Console.WriteLine ("Enter a number: ");
      x = Convert.ToInt32 (Console.ReadLine ());
      Console.WriteLine ("Output:");
      Console.WriteLine ("{0}{0}{0}", x);
      Console.WriteLine ("{0} {0}", x);
      Console.WriteLine ("{0} {0}", x);
      Console.WriteLine ("{0} {0}", x);
      Console.WriteLine ("{0}{0}{0}", x);

      Console.WriteLine ("--------------------------------------");
    }
    public static void DrawTriangle () {
      Console.WriteLine ("------Task-4 Draw Triangle:----");
      Console.WriteLine ("Input a number: ");
      int num = Convert.ToInt32 (Console.ReadLine ());

      Console.WriteLine ("Input the desired width: ");
      int width = Convert.ToInt32 (Console.ReadLine ());
      Console.WriteLine ("Output: ");
      int height = width;
      for (int row = 0; row < height; row++) {
        for (int column = 0; column < width; column++) {
          Console.Write (num);
        }
        Console.WriteLine ();
        width--;
      }
      Console.WriteLine ("--------------------------------------");
    }

    public static void DrawRighAngleTriangle () {
      Console.WriteLine ("------Task-5 Draw RightAngleTriangle:----");
      int i, j, rows;
      Console.Write ("Input number of rows : ");
      rows = Convert.ToInt32 (Console.ReadLine ());
      Console.WriteLine ("Output:");
      for (i = 1; i <= rows; i++) {
        for (j = 1; j <= i; j++)
          Console.Write ("{0}", j);
        Console.Write ("\n");
      }

      Console.WriteLine ("--------------------------------------");
    }
    public static void DrawPyramid () {
      Console.WriteLine ("------Task-6 Draw Pyramid:----");
      int i, j, spaceP, rows, k, t = 1;
      Console.Write ("input number of rows : ");
      rows = Convert.ToInt32 (Console.ReadLine ());
      Console.WriteLine ("Output:");
      spaceP = rows;
      for (i = 1; i <= rows; i++) {
        for (k = spaceP; k >= 1; k--) {
          Console.Write (" ");
        }
        for (j = 1; j <= i; j++)
          Console.Write ("{0} ", t++);
        Console.Write ("\n");
        spaceP--;
      }

      Console.WriteLine ("--------------------------------------");
    }
  }
}